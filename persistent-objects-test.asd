(asdf:defsystem #:persistent-objects-test
  :author "Kamen Tomov"
  :version "0.0.1"
  :serial t
  :depends-on (persistent-objects fiveam)
  :components ((:module "test"
                        :components ((:file "handler-test")))))
