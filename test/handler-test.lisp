(defpackage #:tomov.kamen.persistent-objects-test.istore
  (:use #:cl #:cl-prevalence)
  (:export
   #:get-firstname
   #:get-lastname
   #:managed-person
   #:human
   #:human-sex

   #:animal
   #:animal-sex
   #:ant
   #:get-color))

(defpackage #:tomov.kamen.persistent-objects-test
  (:use
   #:cl
   #:cl-prevalence
   #:tomov.kamen.persistent-objects
   #:tomov.kamen.persistent-objects-test.istore
   #:5am)
  (:export
   #:test-persistent-objects))

(in-package #:tomov.kamen.persistent-objects-test)

(def-suite test-persistent-objects)

(in-suite test-persistent-objects)

(defparameter *test-system-directory* (pathname "/tmp/test-persistent-objects/"))

(defvar *test-system* nil)

(test test-managed-prevalence-start
 (let ((directory *test-system-directory*))
   ;; Throw away any xml files that we find: we want to start from scratch
   (when (probe-file directory)
     (dolist (pathname (directory (merge-pathnames "*.xml" directory)))
       (delete-file pathname)))
   (setf *test-system* (make-prevalence-system directory))
   (is-true *test-system*)))

(test test-create-counter
  "Create a new id counter"
  (execute-transaction (tx-create-id-counter *test-system*))
  (is (zerop (get-root-object *test-system* :id-counter))))


;; A Test CLOS classes

(in-package #:tomov.kamen.persistent-objects-test.istore)

(defclass human (object-with-id)
  ((sex :initarg :sex :initform "tell" :accessor human-sex)))

(defclass managed-person (human)
  ((firstname :initarg :firstname :initform "" :accessor get-firstname)
   (lastname  :initarg :lastname  :initform "" :accessor get-lastname)))

(in-package #:tomov.kamen.persistent-objects-test)

;; Handler defines

(def-persistent-object managed-person *test-system*)

;; Tests
(test test-create-managed-person
  "Create a new test managed-person"
  (let* ((firstname "Jean-Luc")
         (lastname "Picard")
         (managed-person (make-instance 'managed-person :firstname firstname :lastname lastname :sex :male)))
    (is-true (make-persistent *test-system* managed-person))))

(get-id (make-persistent *test-system* (make-instance 'managed-person :firstname "Louis" :lastname "De Funes" :sex :male)))
(tomov.kamen.persistent-objects::get-slots-n-values (make-instance 'managed-person :firstname "Louis" :lastname "De Funes" :sex :male))

(test test-find-all-managed-persons
  (is (equal 1 (length (find-all-objects *test-system* 'managed-person))))
  (let ((managed-person (make-instance 'managed-person :firstname "Louis" :lastname "De Funes" :sex :male)))
    (make-persistent *test-system* managed-person))
  (is (equal 2 (length (find-all-objects *test-system* 'managed-person)))))

(test test-find-managed-person-by-id
  (is (equal (get-lastname (find-object-with-id *test-system* 'managed-person 1)) "Picard"))
  (is (equal (get-lastname (find-object-with-id *test-system* 'managed-person 2)) "De Funes")))

(test test-find-managed-persin-missing
  (is-false (find-object-with-id *test-system* 'managed-person 10))
  (is-false (find-object-with-id *test-system* 'managed-personnn 1)))

(test test-find-with-slot
  (is (equal (get-lastname (find-object-with-slot
                            *test-system*
                            'managed-person
                            'tomov.kamen.persistent-objects-test.istore::firstname "Jean-Luc"))
             "Picard"))
  (is (equal (get-lastname (find-object-with-slot
                            *test-system*
                            'managed-person
                            'tomov.kamen.persistent-objects-test.istore::firstname
                            "Louis"))
             "De Funes")))

(test test-update-managed-person
  (let ((person (find-object-with-id *test-system* 'managed-person 1)))
    (update-persistent
     *test-system*
     person
     `((tomov.kamen.persistent-objects-test.istore::firstname "Luc")
       (tomov.kamen.persistent-objects-test.istore::lastname "Skywalker")))
    (is (equal "Luc" (get-firstname person)))))

(test test-find-managed-person-with-slots
  (let ((managed-person (make-instance 'managed-person :firstname "Louis" :lastname "Vuitton" :sex :male)))
    (make-persistent *test-system* managed-person))
  (let ((found (find-objects-with-slots
                *test-system*
                'managed-person
                `((tomov.kamen.persistent-objects-test.istore::firstname "Louis" equal)))))
    (is (equal 2 (length found)))
    (is (equal "De Funes" (get-lastname (second found))))
    (is (equal "Vuitton" (get-lastname (first found)))))
  (is (equal 3 (length (find-objects-with-slots
                        *test-system*
                        'managed-person
                        `((tomov.kamen.persistent-objects-test.istore::sex :male)))))))

(test test-get-id
  (is (equal 2 (get-id (find-object-with-id *test-system* 'managed-person 2)))))

(test test-delete-managed-person
  (delete-persistent *test-system* (find-object-with-id *test-system* 'managed-person 1))
  (is (equal 2 (length (find-all-objects *test-system* 'managed-person))))
  (delete-persistent *test-system* (find-object-with-id *test-system* 'managed-person 3))
  (is (equal 1 (length (find-all-objects *test-system* 'managed-person))))
  (delete-persistent *test-system* (find-object-with-id *test-system* 'managed-person 2))
  (is (equal 0 (length (find-all-objects *test-system* 'managed-person)))))


(in-package #:tomov.kamen.persistent-objects-test.istore)

(defclass animal (object-with-id)
  ((sex :initarg :sex :initform "tell" :accessor animal-sex)))

(defclass ant (animal)
  ((color :initarg :color :initform "" :accessor get-color)))

(in-package #:tomov.kamen.persistent-objects-test)

;; Handler defines

(def-persistent-object ant *test-system*)

;; Test ants
(test test-create-ant
  (let* ((color :red)
         (ant (make-instance 'ant :color color :sex :male)))
    (is-true (make-persistent *test-system* ant))
    (is (eq (class-of ant) (find-class 'ant)))
    (is (equal (get-color ant) color))))

(test test-update-ant
  (update-persistent
   *test-system*
   (find-object-with-id *test-system* 'ant 4)
   `((tomov.kamen.persistent-objects-test.istore::color :black)))
  (is (equal :black (get-color (find-object-with-id *test-system* 'ant 4)))))

(test test-find-ant-with-slot
  (is (equal (get-color (find-object-with-slot
                         *test-system*
                         'ant
                         'tomov.kamen.persistent-objects-test.istore::sex
                         :male))
             :black)))

(test test-delete-ant
  (delete-persistent *test-system* (find-object-with-id *test-system* 'ant 4))
  (is (equal 0 (length (find-all-objects *test-system* 'ants)))))

;; Test storage
(test test-init
  (let ((directory (pathname "/tmp/test-persistent-objects-1/"))
        instance)
    (when (probe-file directory)
      (dolist (pathname (directory (merge-pathnames "*.xml" directory)))
        (delete-file pathname)))
    (with-storage (instance :dir directory)
      (is-true instance)
      (loop for i to 10
         do (let ((ant (make-instance 'ant :color :pink :sex :male)))
              (is-true (make-persistent instance ant))))
      (is-true (snapshot instance)))))
