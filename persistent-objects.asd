(asdf:defsystem #:persistent-objects
  :author "Kamen Tomov"
  :version "0.0.1"
  :serial t
  :depends-on (cl-prevalence)
  :components ((:module "main"
                        :components ((:file "handler")))))
