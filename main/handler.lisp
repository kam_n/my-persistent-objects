(defpackage tomov.kamen.persistent-objects
  (:use #:common-lisp)
  (:import-from
   #:cl-prevalence
   #:get-id
   #:object-with-id
   #:execute-transaction
   #:find-object-with-id
   #:find-object-with-slot
   #:tx-create-object
   #:tx-delete-object
   #:tx-change-object-slots
   #:find-all-objects

   #:make-prevalence-system
   #:get-root-object
   #:close-open-streams
   #:tx-create-id-counter
   #:snapshot)
  (:export
   #:def-persistent-object
   #:get-id
   #:object-with-id
   #:find-object-with-id
   #:find-all-objects
   #:find-object-with-slot
   #:find-objects-with-slots
   #:print-bound-object-slots
   #:make-persistent
   #:update-persistent
   #:delete-persistent

   #:with-storage
   #:snapshot))

(in-package #:tomov.kamen.persistent-objects)

;; manage storage

(defmacro with-storage ((system &key dir) &body body)
  `(prog2
       (setq ,system (init-storage ,dir))
       ,@body
     (close-storage ,system)))

(defun init-storage (pathname)
  (let ((system (make-prevalence-system pathname)))
    (unless (get-root-object system :id-counter)
      (execute-transaction (tx-create-id-counter system)))
    system))

(defun close-storage (system)
  (close-open-streams system))

;; manage objects
(defgeneric make-persistent (system object))

(defgeneric update-persistent (system object slots-and-values))

(defgeneric delete-persistent (system object))

(defun find-objects-with-slots (system class-name slots-values-and-tests)
  (remove-if-not
   #'(lambda (element)
       (loop for criteria in slots-values-and-tests
          always (funcall (or (third criteria) #'equalp)
                          (slot-value element (first criteria))
                          (second criteria))))
   (find-all-objects system class-name)))

;(assert (find 'object-with-id (closer-mop:class-direct-superclasses ,class-name)))

(defmacro def-persistent-object (class-name &optional (autocommit t))
  "Defines create-thing, find-all-things, delete-thing and default printer."

  `(progn
     (defmethod make-persistent (system (object ,class-name))
       ,(funcall
         (if autocommit 'identity 'cadr)
         `(execute-transaction
           (tx-create-object
            system
            (type-of object)
            (get-slots-n-values object)))))

     (defmethod update-persistent (system (object ,class-name) slots-and-values)
       ,(funcall
         (if autocommit 'identity 'cadr)
         `(execute-transaction
           (tx-change-object-slots
            system
            ',class-name
            (get-id object)
            slots-and-values))))

     (defmethod delete-persistent (system (object ,class-name))
       ,(funcall
         (if autocommit 'identity 'cadr)
         `(execute-transaction
           (tx-delete-object
            system
            ',class-name
            (get-id object)))))

     (defmethod print-object ((object ,class-name) stream)
       (print-bound-object-slots object stream))

     ))


;; General

(defun get-slots-n-values (object)
  (let ((slots (cl-prevalence::serializable-slots
                (slot-makunbound
                 object
                 'cl-prevalence:id))))
    (loop for slot in slots
       if (slot-boundp object slot)
       collect (list slot (slot-value object slot)))))


;; Printing

(defun print-bound-object-slots (object stream)
  (let ((slots (get-bound-slots object)))
    (print-unreadable-object (object stream :type t)
      (format stream "~{~a ~}" (get-values slots object)))))

(defun get-bound-slots (object)
  (mapcan #'(lambda (slot)
              (if (slot-boundp object slot) (list slot)))
          (cl-prevalence::serializable-slots object)))

(defun get-values (slots-n-values val)
  (loop for elem in slots-n-values
     collect (slot-value val elem)))
